(function() {

    'use strict';

    angular
        .module('app')
        .config(config);


    config.$inject = ['$stateProvider', '$urlRouterProvider'];

    function config($stateProvider, $urlRouterProvider) {

        //For any unmatched url
        $urlRouterProvider.otherwise('login');

        //For state url
        $stateProvider
            .state('home', {
                url: '/home',
                templateUrl: 'template/home.html',
                controller: 'ProductController',
                controllerAs: 'vm'
            })
            .state('detail', {
                url: '/store/:productID',
                templateUrl: 'template/detail.html',
                controller: 'ProductController',
                controllerAs: 'vm'
            })
            .state('cart', {
                url: '/cart',
                templateUrl: 'template/cart.html',
                controller: 'ProductController',
                controllerAs: 'vm'
            })
            .state('cart.checkout', {
                url: '/checkout',
                templateUrl: 'template/checkout.html',
                controller: 'PaymentController'
    //                controllerAs: 'vm'
            })
            .state('home.color', {
                url: '/color/:productColor',
                templateUrl: 'template/color.html',
                controller: 'ProductController',
                controllerAs: 'vm'
            })
            .state('login', {
                url: '/login',
                controller: 'LoginController',
                templateUrl: 'template/login.html',
                controllerAs: 'vm'
            })
            .state('register', {
                url: '/register',
                controller: 'RegisterController',
                templateUrl: 'template/register.html',
                controllerAs: 'vm'
            })
    }

})();