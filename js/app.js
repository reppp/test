(function() {
'use strict';

    angular
        .module('app', [
            'routes',
            'ngAnimate',
            'ui.bootstrap',
            'rzModule',
            'ui.filters',
            'ngCart',
            'ngCookies',
            'products.module',
            'security.module',
        ]);

    angular
        .module('routes', ['ui.router']);

    angular
        .module('ui.filters', []);

    angular
        .module('app')
        // .run(function($animate) {
        //     $animate.enabled(true);
        // });
        .run(function($rootScope, $location, $cookies, $http) {
            // keep user logged in after page refresh
            $rootScope.globals = $cookies.getObject('globals') || {};
            if ($rootScope.globals.currentUser) {
                $http.defaults.headers.common['Authorization'] = 'Basic ' + $rootScope.globals.currentUser.authdata;
            }

            $rootScope.$on('$locationChangeStart', function (event, next, current) {
                // redirect to login page if not logged in and trying to access a restricted page
                var restrictedPage = $.inArray($location.path(), ['/login', '/register']) === -1;
                var loggedIn = $rootScope.globals.currentUser;
                if (restrictedPage && !loggedIn) {
                    $location.path('/login');
                }
            });
        })
})();