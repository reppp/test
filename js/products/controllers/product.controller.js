(function() {

    'use strict';

    angular
        .module('products.controllers')
        .controller('ProductController', ProductController);

    ProductController.$inject = ['$scope', '$http', '$filter', '$stateParams', '$state', 'ngCart', 'Product'];

    function ProductController($scope, $http, $filter, $stateParams, $state, ngCart, Product) {
        var vm = this;

        //Setup cart text rate and shipping charge
        ngCart.setTaxRate(7.5);
        ngCart.setShipping(2.99);

        //Pagination
        vm.currentPage = 1;
        vm.itemsPerPage=9;

        vm.id = $stateParams.productID;
        vm.color = $stateParams.productColor;
        vm.state = $state.current.name;
        vm.total = ngCart.totalCost();

        vm.clearCart = function (){
            ngCart.empty();
            vm.total = 0;
        }

        //rzSlider
         vm.minRangeSlider = {
                minValue: 0,
                maxValue: 10000,
                options: {
                    floor: 0,
                    ceil: 10000,
                    step: 1,
                    getPointerColor: function() {
                        return '#007dc2';
                    },
                    getSelectionBarColor: function() {
                        return '#007dc2';
                    },
                    translate: function(value) {
                      return '$' + value;
                    }
                }
        };

        //Reset slider
        vm.resetPrice = function(obj) {
            obj.minValue = 0;
            obj.maxValue = 10000;
            return obj;
        }

        //Filtering product by price range.
        vm.filterRange = function(obj) {
            return obj.price > vm.minRangeSlider.minValue && obj.price <= vm.minRangeSlider.maxValue;
        };


        Product.all(function(data) {
            vm.products = data.data;
            vm.colors = data.data;
            vm.product = $filter('filter')(vm.products, {id: vm.id})[0];
            vm.temp = $filter('filter')(vm.products, {color: vm.product.color});
            vm.relatedProduct = vm.temp.filter(
                function(elem, index){
                    return elem.id != vm.product.id;
                }
            );
            vm.colorProduct = $filter('filter')(vm.products, {color: vm.color});
            vm.items2 = vm.products;
            vm.color2 = vm.colorProduct;
            $scope.$watch('vm.search', function(val)
            {
                vm.products = $filter('filter')(vm.items2, val);
                vm.colorProduct = $filter('filter')(vm.color2, val);

            });
        });

    }

})();