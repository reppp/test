(function() {

	'use strict';

	angular
	    .module('products.module', [
	        'products.controllers',
	        'products.services',
	        'products.filters'
	    ]);

	angular
	    .module('products.controllers', []);

	angular
	    .module('products.services', []);
	    
	angular
	    .module('products.filters', []);

})();