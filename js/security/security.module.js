(function() {

	'use strict';

	angular
	    .module('security.module', [
	        'security.controllers',
	        'security.services'
	    ]);

	angular
	    .module('security.controllers', []);

	angular
	    .module('security.services', []);

})();