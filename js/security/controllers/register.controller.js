(function() {

  'use strict';

  angular
    .module('security.controllers')
    .controller('RegisterController', function(UserService, $location, $rootScope) {
      var vm = this;

      vm.register = register;

      function register() {
          vm.dataLoading = true;
          UserService.Create(vm.user)
              .then(function (response) {
                  if (response.success) {
                      $location.path('/login');
                  } else {
                      vm.dataLoading = false;
                  }
              });
      }
    })
})();